terraform {
  backend "s3" {
    bucket = "zodiacflyer-team3-backend-dev"
    key    = "s3-terraform.tfstate"
    acl    = "bucket-owner-full-control"
    region = "us-east-1"
  }
}