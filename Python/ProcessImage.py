from PIL import Image, ImageDraw, ImageFont


def adding_text(img, text, position, size, colour):
    font = ImageFont.truetype("Game of Thrones.ttf", size)  # Choose the location of the fonts
    draw = ImageDraw.Draw(img) # Call the ImageDraw functions to make the image editable
    draw.text(position, text, font=font, fill=colour) # Write the text in image
    return img


if __name__ == "__main__":
    with Image.open("Test.jpg") as img: # Location of the image and save it into variable 'img'
        print(img.size) # Optional, to make sure the size of the image
        img = adding_text(img, "Arya Stark", (975, 1340),
                          90, (255, 255, 255)) # Choose the img, input the text, location, size, and colour
        img = adding_text(img, "House of Stark",
                          (950, 1450), 75, (255, 255, 255)) # Same as before
        img.save("Test-text.png") # Saving the image