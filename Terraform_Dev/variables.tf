variable "region" {
  type    = string
  default = "us-east-1"
}
variable "account" {
  type    = string
  default = "Sparx/Innovation Launchpad"
}

variable "environment" {
  type    = string
  default = "Dev"
}

variable "team" {
  type    = string
  default = "Team 3"
}

variable "application" {
  type    = string
  default = "Zodiac Flyer"
}

variable "inputbucket" {
  type    = string
  default = "zodiacflyer-team3-input-dev"
}

variable "logbucket" {
  type    = string
  default = "zodiacflyer-team3-logging-dev"
}

variable "websitebucket" {
  type    = string
  default = "zodiacflyer-team3-website-dev"
}
